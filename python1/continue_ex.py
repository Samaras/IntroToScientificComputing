#!/usr/bin/env python3

a = [4, 3, 1, 4, 0, 2, 0, 5, 2, 8]
invsum = 0.0
for ii in a:
    if ii == 0:
        continue
    invsum += 1 / ii
print("The sum of the inverses of the non-zero elements is", invsum)

Numerical Methods
=================

In this part of the course, we'll go through details of the main methods
used for a particular type of problem, including some mathematical background
where appropriate. I'll also show you how some of these methods might be
implemented in Python, though I'll try to make these examples more
understandable rather than more optimized. And in each section we'll include
some examples of how to use Python packages that implement these methods.

Sometimes you will need to implement solutions yourself. Python offers many
ways to do the same thing. Even something as simple as summing over the
elements of a list can be done in a number of ways, some of which will be much
slower than others. If you can think of several ways to do something it's
often best to just try them and see how they compare (keeping in mind it's
only worth spending a lot of time on this if it's in a part of your code
that's taking a lot of time). The
[`%timeit`](http://ipython.readthedocs.io/en/stable/interactive/magics.html#magic-timeit)
ipython/jupyter magic is a useful quick way to do this.

Prerequisites
-------------

You should be comfortable programming in [Python 3](https://www.python.org/),
have some familiarity with [NumPy](https://www.numpy.org/),
[MatPlotLib](https://matplotlib.org/) and [SciPy](https://scipy.org/), and
have access to a computer with all of these installed along with
[jupyter](http://jupyter.org/). The provided course material is primarily in
the form of ipython/jupyter notebooks.

To download the course material, you can clone the git repository with
`git clone https://gitlab.com/eamonn.murray/IntroToScientificComputing.git`.
You could also create a gitlab account, which you will need to do for the
[course assignments](Assignments.md), and [fork the
repository](https://gitlab.com/eamonn.murray/IntroToScientificComputing/forks/new),
which will create a copy in your gitlab account.

I suggest you also `git pull` in your local copy of the course folder at the
beginning of each class so you get the latest version. This part of the course
is in the `numerical_methods` folder. [Other folders](../README.md) contain
material you may also find useful.

A fairly strong mathematical background will also be beneficial, particularly
in linear algebra and calculus. Most methods have some discussion or examples
of their use in the simulation of materials, but is otherwise quite general.

Contents
--------

This will be added to as we progress through the course. The main topics are
divided into directories, each containing an ipython/jupyter notebook with
details of the methods and python examples. While the gitlab web interface
will render the notebooks and mathematical expressions in a readable form,
the cell outputs has not been included, so it's best to open them on your
own computer as a jupyter notebook.

- [Initial Considerations](#initial-considerations)
- [Instructions for Assignments](Assignments.md)
- [Root Finding](01_Root_Finding)
- [Systems of Equations](02_Systems_of_Equations)

Initial Considerations
----------------------

There are a number of things you should keep in mind when choosing a method
for a problem, or when implementing a method.

### Language

In this course, we'll be using Python (version 3). However, choice of language
can have a significant effect on how fast the code you write actually runs.

Rather than thinking of this as some languages are slower than others, it's
more the case that different languages have different sets of constraints that
may or may not allow various optimizations when compiled into machine
instructions. Often this means in practice there can be a trade-off between
user-friendliness and speed when deciding what language to use. And some
languages will generally produce faster executables for certain types of
problems. For example, a Fortran or C implementation of a method will likely
run several orders of magnitude faster than a native Python implementation.

#### Compiled, interpreted and everything in between

Often people distinguish between compiled and interpreted languages. But,
strictly speaking, this is a property of an implementation of a language
rather than some essential property of the language itself.
- Compiled languages usually have a step where you invoke a compiler to
  generate a separate executable file from your source code, for example
  C/C++/Fortran. Once this step is done, you generally no longer need to worry
  about the compiler, and can just run your executable.
- Interpreted languages are executed directly more like a script. For example
  Bash or Perl. These usually run somewhat more slowly than a compiled
  executable, although are usually more flexible and easier to alter.
- Many modern language implementations can do something in between, such as
  Python which generates an intermediate representation when run, each time a
  change to the source file is detected. The intermediate representation is
  not in machine code and can't be directly executed but is in a form that can
  be executed efficiently by an interpreter.
- Another approach that's popular in implementations of many languages is
  what's known as Just-In-Time (JIT) compilation, where the code is compiled
  at run time rather than as a separate step before execution. Implementations
  can combine the advantages (and disadvantages) of both compiled and
  interpreted implementations. There is a JIT compiler for python called
  [PyPy](https://pypy.org/).

#### Static and dynamic typing

- In a statically typed language, the type of each variable is set and its
  compatibility with each expression in which it's used is checked at compile
  time.
    - This means the type of each variable must be declared (typically
      explicitly, but also possibly implicitly) in the source code.
    - A variable that's declared to store e.g. an integer, can only be used to
      store an integer.
- In a dynamically typed language, the types are only checked at runtime. The
  types are associated with objects or values that variables can point at
  rather than the variables themselves.
    - Python is a dynamically typed language.
    - You can assign e.g. an integer to a variable, and later assign a string
      to the same variable without issue.
    - Types are often set and converted automatically, which can be quite
      convenient, but may also not always be what you expect.
- Statically typed languages can usually use better optimizations than
  dynamically typed, and so produce faster executables.
- The [Cython](http://cython.org/) Python compiler allows you to declare
  statically typed variables in a a Python code to produce much faster
  executables.

### Libraries and Python Modules

Of course, you don't need to restrict yourself to a single language when
approaching a problem. As you've seen in your other classes, you can use
Python with Fortran. And it's also common to use Python with C or C++. Indeed,
many numerically intensive modules you might use in Python are in fact
interfaces to Fortran or C libraries rather than native implementations in
Python.

While it's important to understand the methods you use, how they scale, and
their likely strengths and drawbacks, you will rarely need to implement the
numerically intensive part of the solution yourself, as optimized
implementations are almost always available as libraries (or as modules which
allow you to use those libraries in Python).

Libraries are files containing some chunk of code that you can use from your
own codes rather than needing to reproduce yourself. They'll have some well
defined interface, allowing you to call them e.g. as a normal subroutine in a
Fortran code. They can then be linked either statically (built-in to your
executable when your code is compiled), or dynamically (the separate library
executable is called from your compiled code and needs to be locatable on the
machine on which you execute your code). When you load a Python module, you
make some functions available that you can call in you code, such that you
don't need to worry about how they're implemented. As mentioned, many are
actually interfaces to libraries written in some other language. One of the
reasons for Python's popularity is the ease with which you can create modules
that add useful functionality like this.

Scaling and Big O Notation
--------------------------

One of the most important aspects of a method is how it scales. If a method
scales poorly, while it may complete in a tolerable amount of time for a small
problem, a slightly larger problem could take years to complete.

The usual way the scaling of a method is indicated is with what's known as
big O notation. This describes the limiting behaviour of the method as the
size of the problem increases, and can refer both to the time required by the
method or to the space (memory or disk) required by the method. For example,
a method where the number of operations was given by `6*N+4N^2+2N^4` where
N is the size of the problem (e.g. number of atoms simulated) would
have O(n^4). The is because as N gets large, the number of operations is
always less then some constant times n^4. Similarly O(n), implies the method
scales linearly with problem size, while O(1) implies constant scaling
(the number of operations is independent of n).

Assignment Instructions
=======================

[Back to overview](README.md)

Set Up
------

- Create an account on
  [gitlab.com](https://gitlab.com/users/sign_in#register-pane) if you do not
  already have one.
    - Note if you scroll down to the bottom of this page you can choose to
      sign in through another account such as google or bitbucket.
    - This account will also be used for the group programming project in term
      two as bitbucket limits free private repositories to a maximum of 5
      contributors.
- Add an ssh key in the [SSH Keys section of User
  Settings](https://gitlab.com/profile/keys). This will allow you to push
  local repositories on your computer to your gitlab account.
- Create a private repository called "TSMCDT-NumericalMethods-2017" by using
  the new project button, entering the project name in appropriate box, and
  ensuring the visibility level is set as private.
    - This repository will be used for all of your assignments for this
      course.
- Follow the instructions on the next page to create a new repository on your
  laptop or computer.
- In the left hand menu on the gitlab web interface, select "Settings" ->
  "Members".
    - In the box headed "Select members to invite" enter "eamonn.murray" and
      select my account.
    - Under "Choose a role permission" select "Reporter".
    - This will allow me to clone your repository for grading.
- Each assignment will be contained within its own folder: `Assignment1`,
  `Assignment2`, `Assignment3` and `Assignment4`.
- As you create the files required for the assignment you should add them to
  your local git repo and push the changes to the remote gitlab repo.
- I will automatically download a copy of your gitlab repository at the
  assignment deadline for grading.

Assignments
-----------

- Assignments will be posted on the course blackboard page each week,
  typically as a pdf file or jupyter/ipython notebook.
- Where Python code is asked for, these should be created as executable Python
  files suffixed as `.py` and conforming to the Python 3 standard, and
  beginning with the line `#!/usr/bin/env python3`. You should **not** submit
  jupyter notebooks unless specifically requested.
- Please test your code, and functions individually.
- To allow automatic testing, and importing of functions from modules you
  write please ensure:
    - Any code intended to be run when the python script is executed is
      contained within a `main()` function that is called with the usual
      [boilerplate](../python1/README.md#boilerplate-modules-and-using-a-main-function).
    - Functions do not produce plot or print output *by default* unless
      specifically requested.
- Python code should follow [the PEP8 style
  guide](https://www.python.org/dev/peps/pep-0008/) as much as possible. Your
  code should be properly commented. Functions should have docstrings that
  outline what the function does, what the arguments it expects are, and what
  it returns.
- Any Python code you write should only import modules from the standard
  Python library, NumPy, SciPy or MatPlotLib.
- You will not necessarily have covered everything I will ask you to do in the
  courses you have taken so far. Python and the various libraries you are
  using in this course are very well documented online. It is important to
  understand how to search this material to find how best to do something.

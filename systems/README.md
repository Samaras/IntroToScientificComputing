Computer Systems
================

[Full topic list](../README.md)

Computers are electronic devices which can be instructed to perform various
mathematical or logical operations. Having some basic understanding of how the
typical computer components work together to execute a program will help you
use computer resources more effectively.

Binary, Bits and Bytes
----------------------

### Binary
Computers store and process information in binary, as sequences of `1`s and
`0`s. It is important to appreciate that anything stored in a computer will,
at the lowest level, simply be some sequence of 1s and 0s. This has important
implications for the representation of floating point numbers as will be
discussed later.

### Bits
A **bit** refers to a single element of information - either a `1` or a `0`.
This is the smallest building block of storage. The word is not usually
abbreviated, although often a small "b" is used. Network speeds are usually
given in terms of bits. For example, Gigabit Ethernet has a transfer rate of
10^9 bits per second.

### Bytes
A **byte** is a group of 8 bits. It is typical to use bytes when talking about
data on a computer. This is the smallest data unit that is available to a
program. The computer will store data, move data and operate on data in terms
of these 8-bit pieces known as bytes. The symbol for byte is a capital "B".

### Prefixes

There is, unfortunately, a little bit of confusion in terms of the prefixes
used to express large numbers of bits and bytes. Historically, it was common
for people to work with powers of 2 when discussing memory capacities, so for
example, a group of 2^10 bytes, or 1024 bytes was referred to as a kilobyte.
This is in conflict with the SI system of units which defines the kilo prefix
to mean a factor of 1000, so kilobyte should refer to 1000 bytes. This has
lead to the situation where both systems are in common usage, and if you buy
for example computer with a 1 terabyte harddrive, the operating system will
tell you it is in fact 931 gigabytes.

To try to alleviate the confusion this caused it was proposed that the
prefixes referring to powers of 1024 be kibi, mebi, gibi etc. This has had
some traction, although in practice, operating systems will still generally
use e.g. kilobyte to refer to 1024 bytes.

I'll try to use the alternative prefixes with kibibyte (KiB), mebibyte (MiB),
gibibyte (GiB) etc throughout this material although I'll probably still say
kilobyte in class out of habit.

As an example of what this will mean to you in practice: say you want to
understand how long you should expect it to take to transfer a 10 GiB file
using a Gigabit connection. You now know that 10 GiB is 80 Gib, which is 80 *
(1.024^3) = 85.9 Gb. So you can expect it to take at least 85.9 seconds.

Components
----------

Computers come in many shapes and sizes these days, but they will at least
have a processor of some sort that will carry out operations, and some way
store the instructions for the processor. But there are usually many more
components than this.

A typical workstation with the side panel removed is shown in `internals.jpg`:

![Computer internals](internals.jpg)

Lets go through the important parts.

### CPU (Central Processing Unit)
- This is the main workhorse of the computer, carrying out the majority of the
  operations.
- A CPU will have some basic set of operations that are hardwired into it.
  Writing programs directly in terms of these operations is quite difficult.
  Computer programs are generally written in some higher-level language that
  at some point, either automatically or explicitly, gets translated into the
  operations the CPU understands.
- CPUs operate using instruction cycles, where it will fetch an instruction
  from memory, determine what the instruction wants it to do, and then carry
  that out. Modern CPUs can often perform cycles concurrently where the next
  instruction starts to be processed before the current instruction completes.
- CPUs also consist of several components themselves, all on a single
  integrated circuit chip.
- Many CPUs have more than one processor together on the same chip, these are
  usually referred to as *multi-core CPUs*.
- Many CPUs now contain *vector processors*, which allow them to perform
  operations on some 1D array of data simultaneously in parallel.
- CPUs have *registers* which is a relatively small area of very fast storage
  contained in the CPU itself where it keeps the data it is currently working
  on. Usually data here can be accessed within a single CPU cycle, and it can
  typically hold several thousand bytes.
- CPUs have *cache* which is a bigger area of fast storage where it keeps data
  it expects to work on soon. Many modern CPUs will have a number of cache
  areas serving specialised purposes. These will vary in speed, from hundreds
  of GiB/s to tens of GiB/s, and in size from the hundred KiB to hundred MiB
  range.

### RAM (Random-Access Memory)
- This provides fast storage for the computer. The operating system will try
  to store frequently used data here.
- Typically storage is volatile; any data stored in RAM is lost when the
  computer is powered down.
- Sizes are usually at least 4 to 8 GiB; high-end workstations can have over
  100 GiB.
- Speeds can be up to around 10 GiB/s.

### Motherboard
- This is the part that allows all the various components to connect together.
  Often it has many components built in - such as devices to handle graphical
  output, sound output, network connectivity and a USB controller for external
  connected peripherals.
- There will be a slots to fit in the CPU(s), the RAM sticks, and slots for
  peripheral cards that offer various other functionality.

### Hard drive
- This is an area of permanent storage for the computer, with size usually
  around the TiB region.
- Usually this will hold the operating system, and user files.
- Hard-disk drives are the slowest area of storage in typical usage, where
  data is stored magnetically on physically rotating platters. These have can
  have data transfer speeds of up to about 140 MiB/s, but is often much lower,
  particularly if the data is fragmented (different parts spread across
  different physical locations on the disk) which can lead to it dropping to
  less than 1 MiB/s.
- Many modern systems use Solid-State Drives (SSDs) which have no moving parts
  and are much faster in operation, with speeds of around 400 MiB/s. SSDs
  which connect directly to the motherboard as a card can be even faster with
  speeds of several GiB/s.

### Graphics card
- Many computers will have a dedicated card for graphical output. These can
  almost be considered small computers themselves.
- The main work is done by the GPU (Graphics Processing Unit) which is a
  specialized circuit able to very efficiently manipulate computer graphics.
- Graphics cards also have their own built-in specialized memory - usually
  several GiB.
- GPUs typically have a massively parallel architecture, with thousands of of
  small efficient cores. For this reason they are increasingly used to
  accelerate non-graphical applications by performing some intensive part of a
  calculation.

### Everything else

Additionally we have:
- The case - the metal box that holds everything together.
- The power supply - which has a transformer and rectifier to convert the
  mains power from an electrical socket into lower voltage DC as required by
  the various components.
- Fans and heat sinks - the heat sinks draw heat away from the processors and
  the fans circulate air to further aid in cooling the heat-producing
  components.
- DVD drive
- An internal speaker for making annoying beeping sounds
- Various cables connecting everything together
- Usually a desktop computer will have a keyboard and mouse connected for user
  input, and a monitor connected to visualise output.

A labelled version of the picture of the computer internals is shown in
`internals_labelled.jpg`:

![Computer internals with labels](internals_labelled.jpg)

High-Performance Computer Systems
---------------------------------

Many high-performance computer systems are composed of large numbers of
computers (nodes) linked together. Broadly these nodes would be similar to a
desktop pc, but with individual components that are more suited for
continually running intensive calculations, and a case that is designed to
slot into a rack, so that many nodes can be stacked together efficiently.

They will often have CPUs with many more cores than a desktop PC (typically
8-20), and each motherboard may have several physical CPUs slotted in. They
would also have more, faster RAM.

A key element of a HPC system is having some kind of fast interconnect between
the nodes in the cluster. This allows fast communication between nodes for
running large scale (many-node) calculations in parallel. This usually
involves installing a specialised card that connects to the motherboard, with
high-speed cabling connecting each node to each other or to a central hub. One
commonly used technology for this is called IfiniBand, which can enable
transfer speeds of tens of Gbits per second. A more recent technology from
Intel, called Omni-Path, can enable data transfer speeds of up to 100 GBits
per second.

Internal Representation of Data
-------------------------------

As mentioned before, computers store all data in a binary representation, as
sequences of bits (1s and 0s). And in modern PCs, these are grouped together
in sets of 8 bits called bytes. Any programming language you use will allow
you to use different *types* of variables which will use a byte or group of
bytes to store different types of information.

### Integers

Say we want to use a single byte to store integer values. We could let each
bit represent a different power of 2, and add them together:

| 2^ | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
|----|---|---|---|---|---|---|---|---|
| 0  | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |
| 1  | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 1 |
| 2  | 0 | 0 | 0 | 0 | 0 | 0 | 1 | 0 |
| 13 | 0 | 0 | 0 | 0 | 1 | 1 | 0 | 1 |

In this way, we could represent integers from 0 to 255 (2^8-1) in a single
byte. But what if we want to want to represent integers that can be positive
or negative? Now we can use one of those bits to represent the sign of the
integer, and the rest as different powers of two so our most positive
number is 127, and most negative representable number is -128 (since we don't
need to represent `-0` we can get one extra number in the negative range).

Most programming languages will use at least two bytes to represent an
integer by default, but will also have integer types that use more bytes
available.

Recent Python versions will use as many bytes as is necessary to represent
the desired integer, but this behaviour is *not* typical of most programming
languages.

### Characters

The usual way characters are stored is the same as for integers. We can say
each represent 255 different integers in a single byte, so we say each of
these corresponds to a different character.

### Real Numbers: Floating-Point Format

Real numbers are most often represented in what is known as *floating-point*
format. In the "single-precision" version of this scheme a real number is
stored in four bytes (32 bits): a single bit is used for the sign, 8 bits are
used for the (signed) exponent, and the remaining 23 bits are used for the
mantissa.

Note we can effectively have 24 bits of precision in the mantissa by assuming
there is a leading bit that is a `1`. This is since a number in scientific
notation doesn't begin with a zero.

If we want to represent a particular real number. First we convert the number
to binary, noting that binary numbers can have the equivalent of a *decimal*
point too. We can work out that `6.625 = 2^2 + 2^1 + 2^-1 + 2^-3` so it
corresponds to `110.101` in binary. Then we can convert this to scientific
notation: `110.101 = 1.10101 x 2^2`. As mentioned, we don't need to store
the leading `1` before the decimal, so we would store `10101` in for the
mantissa, `0` for the sign bit (this indicates positive), and `10` (2 in
binary) for the exponent.

The example of `6.625` worked out nicely, but it is important to note that
**the majority of real numbers cannot be represented exactly**. In these cases
we use as many bits of precision as are available for the mantissa, so that
the majority of real numbers are implicitly rounded to their nearest
representable number. For example, the decimal number `1.2` cannot be
represented exactly in this system (similar to how many fractions cannot be
represented exactly in decimal, such as 1/3). This can lead to some
non-obvious results when combining real numbers, such as `0.3 - 0.1` giving
`0.19999999999999998` instead of `0.2`.

Many programming languages will offer both 32 bit and 64 bit types for storing
floating point numbers with different levels of precision. But you always need
to be aware that there are many ways in which both the limited amount of
precision for representing the mantissa, and this rounding to the nearest
representable number can result in the loss of precision in a calculation.
Most importantly:
- Subtracting two nearly equal numbers will give a result with significantly
  lower precision.
- When you add two numbers with very large difference in magnitude, depending
  on the scale of the difference and the precision, the smaller number may not
  be large enough to make any difference to the larger number. If you are
  adding many numbers together and want to preserve as much precision as
  possible, it may be best to add small magnitude numbers before larger ones.
- You should never test for equality between two floating point numbers, you
  should instead test that the absolute value of the difference between them
  is less than some tolerance.
